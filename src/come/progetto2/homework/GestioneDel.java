package come.progetto2.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



public class GestioneDel {
	public boolean removeBook(String codiceLibro) {
		GestoreLibri gestLib = new GestoreLibri();
		Libro iLibro = gestLib.ricercaByCodice(codiceLibro);
		
		if (iLibro != null) {
			try {
				Connection conn = Connettore.getInstance().getConnection();
				String queryDel = "Delete from libro where id = ?;";
				PreparedStatement psDel = (PreparedStatement) conn.prepareStatement(queryDel);
				psDel.setInt(1, iLibro.getId());
				int resultDel = psDel.executeUpdate();
				if (resultDel > 0) {
					System.out.println("E' stata eliminata " + resultDel + " riga");
					return true;
				} else {
					System.out.println("ERRORE ELIMINAZIONE");
					return false;
					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				System.out.println("insertPrestitoLibro: " + e.getMessage());
				
			}
		} else if (iLibro == null) {
			System.out.println("Libro non presente!!");
		}
		
			return false;
		
	}
}
