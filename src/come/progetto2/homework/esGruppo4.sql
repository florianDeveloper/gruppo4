drop database if exists gruppo4;
create database gruppo4;
use  gruppo4;

create table libro(
id integer not null primary key auto_increment,
titolo varchar(150) not null, 
codice varchar(20) not null unique
);

create table iscritto(
id integer not null primary key auto_increment,
nome  varchar(150) not null, 
cognome varchar(20) not null,
cod_fis varchar(150) not null,
tes_id varchar(13) unique 
);

create table categoria(
id integer not null primary key auto_increment,
nome  varchar(150) not null
);

create table autore(
id integer not null primary key auto_increment,
nome  varchar(150) not null, 
cognome varchar(20) not null
);

create table libro_categoria(
idLibro integer not null,
idCategoria integer not null,
primary key (idLibro,idCategoria),
foreign key(idLibro) references libro(id) on delete cascade,
foreign key(idCategoria) references categoria(id) on delete cascade
);
SELECT categoria.id,categoria.nome,libro.id,libro.titolo,libro.codice from categoria
join libro_categoria ON categoria.id=libro_categoria.idCategoria
join libro on libro_categoria.idLibro =libro.id where categoria.nome= "giallo";

create table libro_autore(
idLibro integer not null,
idAutore integer not null,
primary key (idLibro,idAutore),
foreign key(idLibro) references libro(id)  on delete cascade,
foreign key(idAutore) references autore(id)  on delete cascade
);


create table prestito(
id integer not null primary key auto_increment,
idIscritto integer not null,
dataRientro varchar(10)  ,
dataInizio varchar(10)  not null,
codice varchar(15),
foreign key(idIscritto) references iscritto(id) on delete cascade
);

create table prestito_libro(
idLibro  integer not null,
idPrestito  integer not null,
primary key (idLibro,idPrestito),
foreign key(idLibro) references libro(id)  on delete cascade,
foreign key(idPrestito) references prestito(id)  on delete cascade
);




insert into categoria(nome) values
("Triller"),
("Giallo"),
("Horror"),
("Romantico"),
("Sci-fi");
-- titolo, codice
SELECT titolo, codice FROM libro
JOIN libro_autore ON libro.id = libro_autore.idLibro
JOIN  autore ON libro_autore.idAutore = autore.id
WHERE autore.nome ="brien"  AND autore.cognome ="gomar";


SELECT idLibro,idCategoria FROM libro_categoria WHERE idLibro = 10;




-- trovare le categorie per ogni libro
SELECT categoria.id,nome,libro.id FROM CATEGORIA
JOIN libro_categoria ON categoria.id = libro_categoria.idCategoria
JOIN libro ON libro_categoria.idLibro = libro.id where libro.id = 1;

-- trovare i autori

SELECT autore.id,nome,cognome, libro.id,libro.titolo FROM autore 
JOIN libro_autore ON autore.id = libro_autore.idAutore
JOIN libro ON libro_autore.idLibro = libro.id 
WHERE libro.id = 13;

Select * from libro_autore;







insert into iscritto (nome, cognome, cod_fis) values ('Jennine', 'Getsham', '86F6B2UA8X');
insert into iscritto (nome, cognome, cod_fis) values ('Tiena', 'Cramp', '1XS3M3AK81');
insert into iscritto (nome, cognome, cod_fis) values ('Tamma', 'Franzonetti', '27HTW0HA4E');
insert into iscritto (nome, cognome, cod_fis) values ('Morris', 'Avramov', 'S5MXJBBTT8');
insert into iscritto (nome, cognome, cod_fis) values ('Stern', 'Johnsson', '92Z69JMTJV');
insert into iscritto (nome, cognome, cod_fis) values ('Agnola', 'Beauchamp', 'THEAA6B4IU');
insert into iscritto (nome, cognome, cod_fis) values ('Polly', 'Laxson', 'OFA95CRLW3');
insert into iscritto (nome, cognome, cod_fis) values ('Julieta', 'Hedaux', '253CG97ZO3');
insert into iscritto (nome, cognome, cod_fis) values ('Rafferty', 'Christie', '08DNBD5P9M');
insert into iscritto (nome, cognome, cod_fis) values ('Rafaela', 'Waslin', 'C87A7VPI6H');
insert into iscritto (nome, cognome, cod_fis) values ('Raven', 'Henkmann', '0XIL47GOHF');
insert into iscritto (nome, cognome, cod_fis) values ('Fredric', 'Orto', '6CFDZT9UHG');
insert into iscritto (nome, cognome, cod_fis) values ('Free', 'Dingsdale', 'AW264BO2YX');
insert into iscritto (nome, cognome, cod_fis) values ('Gena', 'Bonus', 'TM8YS28V2M');
insert into iscritto (nome, cognome, cod_fis) values ('Rockwell', 'McDermot', 'MICSTQMU7W');
insert into iscritto (nome, cognome, cod_fis) values ('Ericha', 'Minget', 'JT1AOK3F25');
insert into iscritto (nome, cognome, cod_fis) values ('Ingra', 'Topp', '3RRS77MFIE');
insert into iscritto (nome, cognome, cod_fis) values ('Austen', 'Backshill', 'APKHHOONZF');
insert into iscritto (nome, cognome, cod_fis) values ('Aurea', 'Delacroux', '7EMHN9N8L1');
insert into iscritto (nome, cognome, cod_fis) values ('Raquela', 'Djokovic', 'P45XIX2L2E');

insert into autore (nome, cognome) values ('Elden', 'Beasleigh');
insert into autore (nome, cognome) values ('Eduard', 'Jupp');
insert into autore (nome, cognome) values ('Prudi', 'Espley');
insert into autore (nome, cognome) values ('Lamond', 'Richardsson');
insert into autore (nome, cognome) values ('Orlan', 'Fidgin');
insert into autore (nome, cognome) values ('Hakeem', 'Lazer');
insert into autore (nome, cognome) values ('Carly', 'Cowderoy');
insert into autore (nome, cognome) values ('Huntlee', 'Lambell');
insert into autore (nome, cognome) values ('Christina', 'Daldry');
insert into autore (nome, cognome) values ('Gaelan', 'Vickerstaff');
insert into autore (nome, cognome) values ('Seline', 'Peniello');
insert into autore (nome, cognome) values ('Kathryne', 'Clyma');
insert into autore (nome, cognome) values ('Wandis', 'Chivers');
insert into autore (nome, cognome) values ('Kaine', 'Poulden');
insert into autore (nome, cognome) values ('Alverta', 'Gaskal');
insert into autore (nome, cognome) values ('Brien', 'Gomar');
insert into autore (nome, cognome) values ('Colan', 'Hatwell');
insert into autore (nome, cognome) values ('Nathanil', 'Pickthorne');
insert into autore (nome, cognome) values ('Neely', 'Steptow');
insert into autore (nome, cognome) values ('Etta', 'Faltskog');

insert into prestito (dataInizio,idIscritto) values ('08/01/2015',1);
insert into prestito (dataInizio,idIscritto) values ('28/07/2016',2);
insert into prestito (dataInizio,idIscritto) values ('25/11/2019',3);
insert into prestito (dataInizio,idIscritto) values ('04/02/2011',4);
insert into prestito (dataInizio,idIscritto) values ('07/12/2017',5);
insert into prestito (dataInizio,idIscritto) values ('22/07/2019',6);
insert into prestito (dataInizio,idIscritto) values ('23/12/2010',7);
insert into prestito (dataInizio,idIscritto) values ('20/08/2011',8);
insert into prestito (dataInizio,idIscritto) values ('30/01/2015',9);
insert into prestito (dataInizio,idIscritto) values ('25/01/2017',10);
insert into prestito (dataInizio,idIscritto) values ('27/08/2020',11);
insert into prestito (dataInizio,idIscritto) values ('11/01/2019',12);
insert into prestito (dataInizio,idIscritto) values ('16/07/2010',13);
insert into prestito (dataInizio,idIscritto) values ('05/02/2014',14);
insert into prestito (dataInizio,idIscritto) values ('08/01/2016',15);
insert into prestito (dataInizio,idIscritto) values ('06/04/2018',16);
insert into prestito (dataInizio,idIscritto) values ('09/09/2018',17);
insert into prestito (dataInizio,idIscritto) values ('18/04/2013',18);
insert into prestito (dataInizio,idIscritto) values ('21/03/2011',19);
insert into prestito (dataInizio,idIscritto) values ('29/12/2018',20);

insert into libro (titolo, codice) values ('Smith', 'JGG659YZUH');
insert into libro (titolo, codice) values ('Renelle', 'M2UP6YLNM9');
insert into libro (titolo, codice) values ('Johnathon', 'LN78I4DGV6');
insert into libro (titolo, codice) values ('Wallie', '4LRAGK3V3P');
insert into libro (titolo, codice) values ('Daphna', 'JJR94ARGQS');
insert into libro (titolo, codice) values ('Conway', '4NX5USLT2Q');
insert into libro (titolo, codice) values ('Jsandye', 'X1KDM2C4JV');
insert into libro (titolo, codice) values ('Diahann', 'QXOV6ERL37');
insert into libro (titolo, codice) values ('Jacquenette', 'QQ2P9GHREU');
insert into libro (titolo, codice) values ('Kanya', 'OJK6QUKPW0');
insert into libro (titolo, codice) values ('Mufi', 'Y2BVOB97IE');
insert into libro (titolo, codice) values ('Salaidh', 'YO3Z8OZPYN');
insert into libro (titolo, codice) values ('Florette', 'B7N0J10DDE');
insert into libro (titolo, codice) values ('Troy', '0COT71J5TQ');
insert into libro (titolo, codice) values ('Vitia', 'NX2VWNLFMA');
insert into libro (titolo, codice) values ('Sherman', '2AAXFDFA0K');
insert into libro (titolo, codice) values ('Neile', 'CUNZOLQNP7');
insert into libro (titolo, codice) values ('Cristen', 'LWV96QTYVN');
insert into libro (titolo, codice) values ('Georgia', '3MYG95RYG3');
insert into libro (titolo, codice) values ('Fred', 'OOX4YLFIKY');
insert into libro (titolo, codice) values ('Kamasutra', 'kama6990');


insert into libro_autore (idLibro, idAutore) values (20, 7);
insert into libro_autore (idLibro, idAutore) values (13, 12);
insert into libro_autore (idLibro, idAutore) values (20, 1);
insert into libro_autore (idLibro, idAutore) values (16, 3);
insert into libro_autore (idLibro, idAutore) values (12, 9);
insert into libro_autore (idLibro, idAutore) values (14, 2);
insert into libro_autore (idLibro, idAutore) values (19, 9);
insert into libro_autore (idLibro, idAutore) values (11, 10);
insert into libro_autore (idLibro, idAutore) values (14, 9);
insert into libro_autore (idLibro, idAutore) values (16, 1);
insert into libro_autore (idLibro, idAutore) values (9, 16);
insert into libro_autore (idLibro, idAutore) values (15, 14);
insert into libro_autore (idLibro, idAutore) values (15, 11);
insert into libro_autore (idLibro, idAutore) values (15, 5);
insert into libro_autore (idLibro, idAutore) values (5, 19);
insert into libro_autore (idLibro, idAutore) values (14, 11);
insert into libro_autore (idLibro, idAutore) values (10, 16);
insert into libro_autore (idLibro, idAutore) values (13, 1);
insert into libro_autore (idLibro, idAutore) values (17, 7);
insert into libro_autore (idLibro, idAutore) values (17, 5);



insert into libro_categoria (idLibro, idCategoria) values (3, 3);
insert into libro_categoria (idLibro, idCategoria) values (9, 5);
insert into libro_categoria (idLibro, idCategoria) values (6, 2);
insert into libro_categoria (idLibro, idCategoria) values (5, 2);
insert into libro_categoria (idLibro, idCategoria) values (20, 5);
insert into libro_categoria (idLibro, idCategoria) values (1, 5);
insert into libro_categoria (idLibro, idCategoria) values (18, 3);


insert into libro_categoria (idLibro, idCategoria) values (15, 1);
insert into libro_categoria (idLibro, idCategoria) values (8, 1);
insert into libro_categoria (idLibro, idCategoria) values (16, 4);
insert into libro_categoria (idLibro, idCategoria) values (12, 5);
insert into libro_categoria (idLibro, idCategoria) values (11, 3);
insert into libro_categoria (idLibro, idCategoria) values (20, 4);
insert into libro_categoria (idLibro, idCategoria) values (7, 5);
insert into libro_categoria (idLibro, idCategoria) values (4, 5);
insert into libro_categoria (idLibro, idCategoria) values (8, 4);
insert into libro_categoria (idLibro, idCategoria) values (7, 3);
insert into libro_categoria (idLibro, idCategoria) values (20, 3);




insert into prestito_libro (idPrestito, idLibro) values (20, 18);
insert into prestito_libro (idPrestito, idLibro) values (13, 14);
insert into prestito_libro (idPrestito, idLibro) values (17, 14);
insert into prestito_libro (idPrestito, idLibro) values (13, 15);
insert into prestito_libro (idPrestito, idLibro) values (3, 16);
insert into prestito_libro (idPrestito, idLibro) values (15, 17);
insert into prestito_libro (idPrestito, idLibro) values (11, 6);
insert into prestito_libro (idPrestito, idLibro) values (13, 8);
insert into prestito_libro (idPrestito, idLibro) values (18, 13);
insert into prestito_libro (idPrestito, idLibro) values (19, 14);
insert into prestito_libro (idPrestito, idLibro) values (1, 13);
insert into prestito_libro (idPrestito, idLibro) values (17, 20);
insert into prestito_libro (idPrestito, idLibro) values (17, 16);
insert into prestito_libro (idPrestito, idLibro) values (8, 12);
insert into prestito_libro (idPrestito, idLibro) values (16, 4);
insert into prestito_libro (idPrestito, idLibro) values (4, 14);
insert into prestito_libro (idPrestito, idLibro) values (14, 4);
insert into prestito_libro (idPrestito, idLibro) values (1, 19);
insert into prestito_libro (idPrestito, idLibro) values (20, 12);


select * from ISCRITTO WHERE NOME = "giovanni";

delete from iscritto where id=1;


