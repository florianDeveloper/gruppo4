package come.progetto2.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestoreLibri {
	
	/**
	 * 
	 * @param codice
	 * @return (libro = null se non ha trovato nulla, Libro trovato)
	 */
	public Libro ricercaByCodice(String codice) {
		int flag = -2;
		Libro Libro = new Libro();
		Libro = null;
		try {
			flag = -1;
			ArrayList<Libro> listaLibro = this.listaLibri();
			for (Libro iLib : listaLibro) {
				if (iLib.getCodice() != null) {
					if (iLib.getCodice().equals(codice)) {
						flag = 1;
						System.out.println("Trovato: "+iLib.toString());
						return  iLib;
						
						
					}
				}
			}
			return Libro;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Libro;
		}

	}
	private ArrayList<Libro> listaLibri() throws SQLException {

		Connection conn = Connettore.getInstance().getConnection();
		String querySel = "select id, titolo, codice from libro";
		PreparedStatement psSel = (PreparedStatement) conn.prepareStatement(querySel);
		ResultSet result = psSel.executeQuery();
		ArrayList<Libro> elencoLibro = new ArrayList<Libro>();

		while (result.next()) {
			Libro libTemp = new Libro();
			libTemp.setId(result.getInt(1));
			libTemp.setTitolo(result.getString(2));
			libTemp.setCodice(result.getString(3));
			elencoLibro.add(libTemp);
		}
		return elencoLibro;

	}

}
