package come.progetto2.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.sql.Statement;
import java.time.LocalDate;

public class GestorePrestito {

	public void stampaCarrello(ArrayList<Libro> elencoLibri) {
		int i = 1;
		for (Libro iLibro : elencoLibri) {

			System.out.println(i + ") " + iLibro.toString());
			i++;
		}
	}

	private boolean ricercaCarrello(String codice, ArrayList<Libro> elencoLibri) {
		boolean flag = true;
		for (Libro iLibro : elencoLibri) {
			if (iLibro.getCodice().equals(codice)) {
				System.out.println("Libro gia presente nel carrello");
				flag = false;
			}
		}
		return flag;
	}

	public void newBook(String codice, ArrayList<Libro> elencoLibri) {
		GestoreLibri gestLib = new GestoreLibri();
		Libro newBook = gestLib.ricercaByCodice(codice);
		if (newBook != null) {

			if (ricercaCarrello(codice, elencoLibri)) {
				elencoLibri.add(newBook);
				System.out.println("Libro aggiunto");
			}
		} else {
			System.out.println("Libro non trovato!!");
		}

	}

	public boolean finalizzaPrestito(ArrayList<Libro> elencoLibri, String codiceIscritto) {
		boolean flag = false;
		GestoreIscritti gestIsc = new GestoreIscritti();
		Iscritto utente = gestIsc.ricercaByCod(codiceIscritto);
		int ID = utente.getId();
		try {
			Random random = new Random();
			int randNumb = random.nextInt(999999999); // randominteger
			String newCodice = "PRE" + String.format("%09d", randNumb);
			Prestito presTemp = new Prestito();
			presTemp.setData_acc(LocalDate.now().toString());
			presTemp.setIscritto(utente);
			presTemp.setCodice(newCodice);
			Connection connessione = Connettore.getInstance().getConnection();
			String query = "insert into prestito (idIscritto, dataInizio, codice) VALUES (?, ?, ?);";
			PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, ID);
			ps.setString(2, LocalDate.now().toString());
			ps.setString(3, newCodice);
			
			ps.executeUpdate();
			
			ResultSet risultato = ps.getGeneratedKeys();
			risultato.next();
			int idPrestito = risultato.getInt(1);
			presTemp.setId(idPrestito);
			
			if(insertPrestitoLibro(idPrestito, elencoLibri)) {
				System.out.println("Prestito finalizzato!");
				return true;
			}
			else
				return false;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
			return flag;
		}

	}

	private boolean insertPrestitoLibro(int idPrestito, ArrayList<Libro> elencoLibri) {
		try {
			Connection connessione = Connettore.getInstance().getConnection();

			for (Libro iLibro : elencoLibri) {
				int idLibro = iLibro.getId();
				String query = "insert into prestito_libro (idLibro, idPrestito) values (?, ?);";
				PreparedStatement ps;

				ps = (PreparedStatement) connessione.prepareStatement(query);
				ps.setInt(1, idLibro);
				ps.setInt(2, idPrestito);
				ps.executeUpdate();
				
			}
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("insertPrestitoLibro: " + e.getMessage());
			e.printStackTrace();
			return false;
		}

	}
}
