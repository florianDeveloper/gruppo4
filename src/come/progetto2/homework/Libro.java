package come.progetto2.homework;


import java.util.ArrayList;

public class Libro {
	private int id;
	private String titolo;
	private String codice;
	private ArrayList<Autore> elenco_aut = new ArrayList<Autore>();
	private ArrayList<Categoria> elenco_cat = new ArrayList<Categoria>();
	
	Libro(){
		
	}
	
	Libro(String titolo, String codice, ArrayList<Autore> elenco_aut, ArrayList<Categoria> elenco_cat) {
		this.titolo = titolo;
		this.codice = codice;
		this.elenco_aut = elenco_aut;
		this.elenco_cat = elenco_cat;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public ArrayList<Autore> getElenco_aut() {
		return elenco_aut;
	}
	public void setElenco_aut(ArrayList<Autore> elenco_aut) {
		this.elenco_aut = elenco_aut;
	}
	public ArrayList<Categoria> getElenco_cat() {
		return elenco_cat;
	}
	public void setElenco_cat(ArrayList<Categoria> elenco_cat) {
		this.elenco_cat = elenco_cat;
	}
	@Override
	public String toString() {
		return "Libro [id=" + id + ", titolo=" + titolo + ", codice=" + codice + ", elenco_aut=" + elenco_aut
				+ ", elenco_cat=" + elenco_cat + "]";
	}
	
	
	
	
}
