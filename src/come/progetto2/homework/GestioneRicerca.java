package come.progetto2.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class GestioneRicerca {
	


public void searchByAutore(String nomeAutore,String CognomeAutore) throws SQLException {
		
		
		Connection conn = Connettore.getInstance().getConnection();
		
		String sqlSearchAu = "SELECT libro.id, titolo, codice FROM libro JOIN libro_autore ON libro.id = libro_autore.idLibro "
				+ " JOIN  autore ON libro_autore.idAutore = autore.id "
				+ "WHERE autore.nome = ?  AND autore.cognome = ?";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sqlSearchAu);
		ps.setString(1, nomeAutore);
		ps.setString(2, CognomeAutore);
		
		
		ResultSet risultati =ps.executeQuery();
		ArrayList<Libro> ricercaByAu= new ArrayList<Libro>();
		
		while(risultati.next()) {
			Libro libTemp = new Libro();
			libTemp.setId(risultati.getInt(1));
			libTemp.setTitolo(risultati.getString(2));
			libTemp.setCodice(risultati.getString(3));
			libTemp.setElenco_aut(this.getElencoById(risultati.getInt(1)));
			libTemp.setElenco_cat(this.getElencoCategById(risultati.getInt(1)));
			
			ricercaByAu.add(libTemp);
			
		}
		this.stampaArraylistLibri(ricercaByAu);
		
		
		
		
		
	}
public ArrayList<Libro> ricercaCategorie(String nomeCateg) throws SQLException {
	//fare una select 
	Connection conn = Connettore.getInstance().getConnection();
	
	String sqlSearchbycategorie ="SELECT categoria.id,libro.id,libro.titolo,libro.codice from categoria "+
			" join libro_categoria ON categoria.id=libro_categoria.idCategoria "+
			" join libro on libro_categoria.idLibro =libro.id where categoria.nome= ?";
	
	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sqlSearchbycategorie);
	ps.setString(1, nomeCateg);
	ResultSet result =  ps.executeQuery();
	
	ArrayList<Libro> elencoLibri = new ArrayList<Libro>();
	while(result.next()) {
		Libro libTemp = new Libro ();
		libTemp.setCodice(result.getString(4));
		libTemp.setId(result.getInt(2));
		libTemp.setTitolo(result.getString(3));
		elencoLibri.add(libTemp);
	}
	
	return elencoLibri;
	
}
	


private ArrayList<Autore> getElencoById(int int1) throws SQLException {
	Connection conn = Connettore.getInstance().getConnection();
		
		String sqlSearchAubyid ="SELECT autore.id,nome,cognome, libro.id,libro.titolo FROM autore "
				+"JOIN libro_autore ON autore.id = libro_autore.idAutore "
				+" JOIN libro ON libro_autore.idLibro = libro.id "
				+" WHERE libro.id = ?";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sqlSearchAubyid);
		ps.setInt(1, int1);
		ResultSet result =  ps.executeQuery();
		
		ArrayList<Autore> elencoAutore = new ArrayList<Autore>();
		while(result.next()) {
			Autore temAut= new Autore();
			temAut.setId(result.getInt(1));
			temAut.setNome(result.getString(2));
			temAut.setCognome(result.getString(3));
			
			
			
			elencoAutore.add(temAut);
		}
		return elencoAutore;
}
private ArrayList<Categoria> getElencoCategById(int int1) throws SQLException {
	//fare una select 
	Connection conn = Connettore.getInstance().getConnection();
	
	String sqlSearchAu ="SELECT categoria.id,nome,libro.titolo FROM CATEGORIA "+
			" JOIN libro_categoria ON categoria.id = libro_categoria.idCategoria "+
			" JOIN libro ON libro_categoria.idLibro = libro.id where libro.id = ?";
	
	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(sqlSearchAu);
	ps.setInt(1, int1);
	ResultSet result =  ps.executeQuery();
	
	ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
	while(result.next()) {
		Categoria temCat= new Categoria();
		temCat.setId(result.getInt(1));
		temCat.setNome(result.getString(2));
		
		
		
		elencoCategorie.add(temCat);
	}
	return elencoCategorie;
	
}

private void stampaArraylistLibri(ArrayList<Libro> arrayDaStampare) {
	for(int i = 0;i<arrayDaStampare.size();i++) {
		Libro temp = arrayDaStampare.get(i);
		System.out.println(temp.toString());
		
	}
	
}
	
	

}
