package come.progetto2.homework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GestoreIscritti {

	public Iscritto ricercaByCod(String codice) {

		Iscritto utente = new Iscritto();
		utente = null;
		try {

			ArrayList<Iscritto> listaIsc = this.listaIscritti();
			for (Iscritto iIsc : listaIsc) {
				if (iIsc.getTes_cli() != null) {
					if (iIsc.getTes_cli().equals(codice)) {
						return iIsc;

					}
				}
			}
			return utente;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return utente;
		}

	}
	
	public boolean ricerca(String codice) {
		Iscritto utente = ricercaByCod(codice);
		if(utente != null) {
			System.out.println("Utente: "+utente.toString() );
			return true;
		}
		else 
			return false;
	}

	private ArrayList<Iscritto> listaIscritti() throws SQLException {

		Connection conn = Connettore.getInstance().getConnection();
		String querySel = "select id, nome, cognome, cod_fis, tes_id from iscritto";
		PreparedStatement psSel = (PreparedStatement) conn.prepareStatement(querySel);
		ResultSet result = psSel.executeQuery();
		ArrayList<Iscritto> elencoIsc = new ArrayList<Iscritto>();

		while (result.next()) {
			Iscritto iscTemp = new Iscritto();
			iscTemp.setId(result.getInt(1));
			iscTemp.setNome(result.getString(2));
			iscTemp.setCognome(result.getString(3));
			iscTemp.setCod_fis(result.getString(4));
			iscTemp.setTes_cli(result.getString(5));

			elencoIsc.add(iscTemp);
		}
		return elencoIsc;

	}

	private void stampa(ArrayList<Iscritto> list) {
		for (Iscritto iIns : list) {
			System.out.println(iIns.toString());
		}

	}

}
